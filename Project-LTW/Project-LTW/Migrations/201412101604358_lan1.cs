namespace Project_LTW.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.DiaDanhs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 500),
                        Body = c.String(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DanhGiaDD = c.Single(nullable: false),
                        UserProfileID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(nullable: false, maxLength: 500),
                        DateCreated = c.DateTime(nullable: false),
                        Author = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Loai = c.Int(nullable: false),
                        DiaDanhID = c.Int(nullable: false),
                        TrungTamID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DiaDanhs", t => t.DiaDanhID, cascadeDelete: true)
                .ForeignKey("dbo.TrungTams", t => t.TrungTamID, cascadeDelete: true)
                .Index(t => t.DiaDanhID)
                .Index(t => t.TrungTamID);
            
            CreateTable(
                "dbo.TrungTams",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 500),
                        Body = c.String(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DanhGiaTT = c.Single(nullable: false),
                        UserProfileID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
            CreateTable(
                "dbo.Ratings",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TrungTamID = c.Int(nullable: false),
                        DiaDanhID = c.Int(nullable: false),
                        Diem = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.TrungTams", t => t.TrungTamID, cascadeDelete: true)
                .ForeignKey("dbo.DiaDanhs", t => t.DiaDanhID, cascadeDelete: true)
                .Index(t => t.TrungTamID)
                .Index(t => t.DiaDanhID);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Content = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DangBaiMois",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 500),
                        Body = c.String(nullable: false),
                        Tagg = c.String(),
                        UserProfileID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
            CreateTable(
                "dbo.Tours",
                c => new
                    {
                        TourID = c.Int(nullable: false, identity: true),
                        TenTour = c.String(nullable: false, maxLength: 500),
                        NgayDang = c.DateTime(nullable: false),
                        NoiDi = c.String(nullable: false, maxLength: 500),
                        NoiDen = c.String(nullable: false, maxLength: 500),
                        NgayKhoiHanh = c.DateTime(nullable: false),
                        SoChoConNhan = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TourID);
            
            CreateTable(
                "dbo.Tag_DiaDanh",
                c => new
                    {
                        DiaDanhID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DiaDanhID, t.TagID })
                .ForeignKey("dbo.DiaDanhs", t => t.DiaDanhID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.DiaDanhID)
                .Index(t => t.TagID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Tag_DiaDanh", new[] { "TagID" });
            DropIndex("dbo.Tag_DiaDanh", new[] { "DiaDanhID" });
            DropIndex("dbo.DangBaiMois", new[] { "UserProfile_UserId" });
            DropIndex("dbo.Ratings", new[] { "DiaDanhID" });
            DropIndex("dbo.Ratings", new[] { "TrungTamID" });
            DropIndex("dbo.TrungTams", new[] { "UserProfile_UserId" });
            DropIndex("dbo.Comments", new[] { "TrungTamID" });
            DropIndex("dbo.Comments", new[] { "DiaDanhID" });
            DropIndex("dbo.DiaDanhs", new[] { "UserProfile_UserId" });
            DropForeignKey("dbo.Tag_DiaDanh", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Tag_DiaDanh", "DiaDanhID", "dbo.DiaDanhs");
            DropForeignKey("dbo.DangBaiMois", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.Ratings", "DiaDanhID", "dbo.DiaDanhs");
            DropForeignKey("dbo.Ratings", "TrungTamID", "dbo.TrungTams");
            DropForeignKey("dbo.TrungTams", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.Comments", "TrungTamID", "dbo.TrungTams");
            DropForeignKey("dbo.Comments", "DiaDanhID", "dbo.DiaDanhs");
            DropForeignKey("dbo.DiaDanhs", "UserProfile_UserId", "dbo.UserProfile");
            DropTable("dbo.Tag_DiaDanh");
            DropTable("dbo.Tours");
            DropTable("dbo.DangBaiMois");
            DropTable("dbo.Tags");
            DropTable("dbo.Ratings");
            DropTable("dbo.TrungTams");
            DropTable("dbo.Comments");
            DropTable("dbo.DiaDanhs");
            DropTable("dbo.UserProfile");
        }
    }
}
