namespace Project_LTW.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tours", "Gia", c => c.Long(nullable: false));
            AddColumn("dbo.Tours", "UrlTour", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tours", "UrlTour");
            DropColumn("dbo.Tours", "Gia");
        }
    }
}
