namespace Project_LTW.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.RatingTTs", "Diem", c => c.Single());
            AlterColumn("dbo.Ratings", "Diem", c => c.Single());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Ratings", "Diem", c => c.Single(nullable: false));
            AlterColumn("dbo.RatingTTs", "Diem", c => c.Single(nullable: false));
        }
    }
}
