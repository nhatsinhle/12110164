namespace Project_LTW.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan4 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.TrungTams", "DanhGiaTT");
            DropColumn("dbo.DiaDanhs", "DanhGiaDD");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DiaDanhs", "DanhGiaDD", c => c.Single(nullable: false));
            AddColumn("dbo.TrungTams", "DanhGiaTT", c => c.Single(nullable: false));
        }
    }
}
