namespace Project_LTW.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Ratings", "TrungTamID", "dbo.TrungTams");
            DropIndex("dbo.Ratings", new[] { "TrungTamID" });
            CreateTable(
                "dbo.RatingTTs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TrungTamID = c.Int(nullable: false),
                        Diem = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.TrungTams", t => t.TrungTamID, cascadeDelete: true)
                .Index(t => t.TrungTamID);
            
            DropColumn("dbo.Ratings", "TrungTamID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Ratings", "TrungTamID", c => c.Int(nullable: false));
            DropIndex("dbo.RatingTTs", new[] { "TrungTamID" });
            DropForeignKey("dbo.RatingTTs", "TrungTamID", "dbo.TrungTams");
            DropTable("dbo.RatingTTs");
            CreateIndex("dbo.Ratings", "TrungTamID");
            AddForeignKey("dbo.Ratings", "TrungTamID", "dbo.TrungTams", "ID", cascadeDelete: true);
        }
    }
}
