﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project_LTW.Models;

namespace Project_LTW.Controllers
{
    [Authorize]
    public class DiaDanhController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /DiaDanh/
        [AllowAnonymous]
        public ActionResult Index(string Publishers, string strSearch)
        {
            var diadanhs = db.DiaDanhs.Include(p => p.UserProfile);
            //return View(posts.ToList());
            //Select all Book records
            var diadanh = from b in db.DiaDanhs

                       select b;

            //Get list of Book publisher
            var publisherList = from c in diadanh
                                orderby c.Title
                                select c.Title;

            //Set distinct list of publishers in ViewBag property
            //ViewBag.Publishers = new SelectList(publisherList.Distinct());

            //Search records by Book Name 
            if (!string.IsNullOrEmpty(strSearch))
                diadanh = diadanh.Where(m => m.Title.Contains(strSearch));
            return View(diadanh);
            //return View(db.DiaDanhs.ToList());
        }

        //
        // GET: /DiaDanh/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            DiaDanh diadanh = db.DiaDanhs.Find(id);
            if (diadanh == null)
            {
                return HttpNotFound();
            }
            ViewData["idtrungtam"] = 1;
            ViewData["iddiadanh"] = id;
            ViewData["Loai"] = 1;
            return View(diadanh);
        }

        //
        // GET: /DiaDanh/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /DiaDanh/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(DiaDanh diadanh,string Content)
        {
            if (ModelState.IsValid)
            {
                //var ds = from n in db.Ratings
                //         where n.DiaDanhID == diadanh.ID
                //      group n by n.DiaDanhID into g
                //         select new { TB = g.Average(x => x.Diem) };
                //diadanh.DanhGiaDD = db.Ratings.Select
                diadanh.DateCreate = DateTime.Now;
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).Where(y => y.UserName == User.Identity.Name).Single().UserId;
                diadanh.UserProfileID = userid;
                //Tao list các tag
                List<Tag> Tags = new List<Tag>();
                //tach cac Tag theo dau ,
                string[] TagContent = Content.Split(',');
                //Lập các tag vừa tách
                foreach (string item in TagContent)
                {
                    //Tìm xem tag content đã có hay chưa
                    Tag TagExists = null;
                    var ListTag = db.Tag.Where(y => y.Content.Equals(item.Trim()));
                    if (ListTag.Count() > 0)
                    {
                        //nếu có tag rồi thì add thêm Post vào
                        TagExists = ListTag.First();
                        TagExists.DiaDanhs.Add(diadanh);
                    }
                    else
                    {
                        //nếu chưa có tag thì tạo mới
                        TagExists = new Tag();
                        TagExists.Content = item.Trim();
                        TagExists.DiaDanhs = new List<DiaDanh>();
                        TagExists.DiaDanhs.Add(diadanh);
                    }
                    //add vào List các tag
                    Tags.Add(TagExists);
                }
                //Gán List Tag cho Post
                diadanh.Tags = Tags;
                db.DiaDanhs.Add(diadanh);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(diadanh);
        }

        //
        // GET: /DiaDanh/Edit/5

        public ActionResult Edit(int id = 0)
        {
            DiaDanh diadanh = db.DiaDanhs.Find(id);
            if (diadanh == null)
            {
                return HttpNotFound();
            }
            return View(diadanh);
        }

        //
        // POST: /DiaDanh/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(DiaDanh diadanh)
        {
            if (ModelState.IsValid)
            {
                diadanh.DateCreate = DateTime.Now;
                db.Entry(diadanh).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(diadanh);
        }

        //
        // GET: /DiaDanh/Delete/5

        public ActionResult Delete(int id = 0)
        {
            DiaDanh diadanh = db.DiaDanhs.Find(id);
            if (diadanh == null)
            {
                return HttpNotFound();
            }
            return View(diadanh);
        }

        //
        // POST: /DiaDanh/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DiaDanh diadanh = db.DiaDanhs.Find(id);
            db.DiaDanhs.Remove(diadanh);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}