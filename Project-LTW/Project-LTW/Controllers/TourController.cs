﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project_LTW.Models;

namespace Project_LTW.Controllers
{
    public class TourController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Tour/

        public ActionResult Index()
        {
            return View(db.Tours.ToList());
        }

        //
        // GET: /Tour/Details/5

        public ActionResult Details(int id = 0)
        {
            Tour tour = db.Tours.Find(id);
            if (tour == null)
            {
                return HttpNotFound();
            }
            return View(tour);
        }

        //
        // GET: /Tour/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Tour/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tour tour)
        {
            if (ModelState.IsValid)
            {
                tour.NgayDang = DateTime.Now;
                db.Tours.Add(tour);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tour);
        }

        //
        // GET: /Tour/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Tour tour = db.Tours.Find(id);
            if (tour == null)
            {
                return HttpNotFound();
            }
            return View(tour);
        }

        //
        // POST: /Tour/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tour tour)
        {
            if (ModelState.IsValid)
            {
                tour.NgayDang = DateTime.Now;
                db.Entry(tour).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tour);
        }

        //
        // GET: /Tour/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Tour tour = db.Tours.Find(id);
            if (tour == null)
            {
                return HttpNotFound();
            }
            return View(tour);
        }

        //
        // POST: /Tour/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tour tour = db.Tours.Find(id);
            db.Tours.Remove(tour);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}