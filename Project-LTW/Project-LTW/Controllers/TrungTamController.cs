﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project_LTW.Models;

namespace Project_LTW.Controllers
{
    [Authorize]
    public class TrungTamController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /TrungTam/
        [AllowAnonymous]
        public ActionResult Index(string Publishers, string strSearch)
        {
            var trungtams = db.TrungTam.Include(p => p.UserProfile);
            //return View(posts.ToList());
            //Select all Book records
            var trungtam = from b in db.TrungTam

                          select b;

            //Get list of Book publisher
            var publisherList = from c in trungtam
                                orderby c.Title
                                select c.Title;

            //Set distinct list of publishers in ViewBag property
            //ViewBag.Publishers = new SelectList(publisherList.Distinct());

            //Search records by Book Name 
            if (!string.IsNullOrEmpty(strSearch))
                trungtam = trungtam.Where(m => m.Title.Contains(strSearch));
            return View(trungtam);
            //return View(db.DiaDanhs.ToList());
        }

        //
        // GET: /TrungTam/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            TrungTam trungtam = db.TrungTam.Find(id);
            if (trungtam == null)
            {
                return HttpNotFound();
            }
            ViewData["idtrungtam"] = id;
            ViewData["iddiadanh"] = 1;
            ViewData["Loai"] = 2;
            return View(trungtam);
        }

        //
        // GET: /TrungTam/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /TrungTam/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(TrungTam trungtam)
        {
            if (ModelState.IsValid)
            {
                trungtam.DateCreate = DateTime.Now;
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).Where(y => y.UserName == User.Identity.Name).Single().UserId;
                trungtam.UserProfileID = userid;
                db.TrungTam.Add(trungtam);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(trungtam);
        }

        //
        // GET: /TrungTam/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TrungTam trungtam = db.TrungTam.Find(id);
            if (trungtam == null)
            {
                return HttpNotFound();
            }
            return View(trungtam);
        }

        //
        // POST: /TrungTam/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(TrungTam trungtam)
        {
            if (ModelState.IsValid)
            {
                trungtam.DateCreate = DateTime.Now;
                db.Entry(trungtam).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(trungtam);
        }

        //
        // GET: /TrungTam/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TrungTam trungtam = db.TrungTam.Find(id);
            if (trungtam == null)
            {
                return HttpNotFound();
            }
            return View(trungtam);
        }

        //
        // POST: /TrungTam/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TrungTam trungtam = db.TrungTam.Find(id);
            db.TrungTam.Remove(trungtam);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}