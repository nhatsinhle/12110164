﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project_LTW.Models;

namespace Project_LTW.Controllers
{
    [Authorize]
    public class DangBaiMoiController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /DangBaiMoi/

        public ActionResult Index()
        {
            return View(db.DangBaiMois.ToList());
        }

        //
        // GET: /DangBaiMoi/Details/5

        public ActionResult Details(int id = 0)
        {
            DangBaiMoi dangbaimoi = db.DangBaiMois.Find(id);
            if (dangbaimoi == null)
            {
                return HttpNotFound();
            }
            return View(dangbaimoi);
        }

        //
        // GET: /DangBaiMoi/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /DangBaiMoi/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(DangBaiMoi dangbaimoi)
        {
            if (ModelState.IsValid)
            {
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).Where(y => y.UserName == User.Identity.Name).Single().UserId;
                dangbaimoi.UserProfileID = userid;
                string[] tagg = dangbaimoi.Tagg.Split(',');
                db.DangBaiMois.Add(dangbaimoi);
                db.SaveChanges();
                return PartialView("_viewDangBai");
            }

            return View(dangbaimoi);
        }

        //
        // GET: /DangBaiMoi/Edit/5

        public ActionResult Edit(int id = 0)
        {
            DangBaiMoi dangbaimoi = db.DangBaiMois.Find(id);
            if (dangbaimoi == null)
            {
                return HttpNotFound();
            }
            return View(dangbaimoi);
        }

        //
        // POST: /DangBaiMoi/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DangBaiMoi dangbaimoi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dangbaimoi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(dangbaimoi);
        }

        //
        // GET: /DangBaiMoi/Delete/5

        public ActionResult Delete(int id = 0)
        {
            DangBaiMoi dangbaimoi = db.DangBaiMois.Find(id);
            if (dangbaimoi == null)
            {
                return HttpNotFound();
            }
            return View(dangbaimoi);
        }

        //
        // POST: /DangBaiMoi/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DangBaiMoi dangbaimoi = db.DangBaiMois.Find(id);
            db.DangBaiMois.Remove(dangbaimoi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}