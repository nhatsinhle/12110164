﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project_LTW.Models;

namespace Project_LTW.Controllers
{
    public class RatingTTController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /RatingTT/

        public ActionResult Index()
        {
            var ratingtts = db.RatingTTs.Include(r => r.TrungTams);
            return View(ratingtts.ToList());
        }

        //
        // GET: /RatingTT/Details/5

        public ActionResult Details(int id = 0)
        {
            RatingTT ratingtt = db.RatingTTs.Find(id);
            if (ratingtt == null)
            {
                return HttpNotFound();
            }
            return View(ratingtt);
        }

        //
        // GET: /RatingTT/Create

        public ActionResult Create()
        {
            ViewBag.TrungTamID = new SelectList(db.TrungTam, "ID", "Title");
            return View();
        }

        //
        // POST: /RatingTT/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RatingTT ratingtt,int idtrungtam)
        {
            if (ModelState.IsValid)
            {
                ratingtt.TrungTamID = idtrungtam;
                db.RatingTTs.Add(ratingtt);
                db.SaveChanges();
                //return RedirectToAction("Index");
                return PartialView("viewRating");
            }

            ViewBag.TrungTamID = new SelectList(db.TrungTam, "ID", "Title", ratingtt.TrungTamID);
            return View(ratingtt);
        }

        //
        // GET: /RatingTT/Edit/5

        public ActionResult Edit(int id = 0)
        {
            RatingTT ratingtt = db.RatingTTs.Find(id);
            if (ratingtt == null)
            {
                return HttpNotFound();
            }
            ViewBag.TrungTamID = new SelectList(db.TrungTam, "ID", "Title", ratingtt.TrungTamID);
            return View(ratingtt);
        }

        //
        // POST: /RatingTT/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RatingTT ratingtt)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ratingtt).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TrungTamID = new SelectList(db.TrungTam, "ID", "Title", ratingtt.TrungTamID);
            return View(ratingtt);
        }

        //
        // GET: /RatingTT/Delete/5

        public ActionResult Delete(int id = 0)
        {
            RatingTT ratingtt = db.RatingTTs.Find(id);
            if (ratingtt == null)
            {
                return HttpNotFound();
            }
            return View(ratingtt);
        }

        //
        // POST: /RatingTT/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RatingTT ratingtt = db.RatingTTs.Find(id);
            db.RatingTTs.Remove(ratingtt);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}