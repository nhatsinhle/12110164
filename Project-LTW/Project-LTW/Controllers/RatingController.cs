﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project_LTW.Models;

namespace Project_LTW.Controllers
{
    public class RatingController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Rating/

        public ActionResult Index()
        {
            var ratings = db.Ratings.Include(r => r.DiaDanhs);
            return View(ratings.ToList());
        }

        //
        // GET: /Rating/Details/5

        public ActionResult Details(int id = 0)
        {
            Rating rating = db.Ratings.Find(id);
            if (rating == null)
            {
                return HttpNotFound();
            }
            return View(rating);
        }

        //
        // GET: /Rating/Create

        public ActionResult Create()
        {
            ViewBag.TrungTamID = new SelectList(db.TrungTam, "ID", "Title");
            ViewBag.DiaDanhID = new SelectList(db.DiaDanhs, "ID", "Title");
            return View();
        }

        //
        // POST: /Rating/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Rating rating,int iddiadanh)
        {
            if (ModelState.IsValid)
            {
                rating.DiaDanhID = iddiadanh;
                db.Ratings.Add(rating);
                db.SaveChanges();
                return RedirectToAction("Details/" + iddiadanh, "DiaDanh");
                //return RedirectToAction("Index");
            }

            ViewBag.DiaDanhID = new SelectList(db.DiaDanhs, "ID", "Title", rating.DiaDanhID);
            return View(rating);
        }

        //
        // GET: /Rating/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Rating rating = db.Ratings.Find(id);
            if (rating == null)
            {
                return HttpNotFound();
            }
            ViewBag.DiaDanhID = new SelectList(db.DiaDanhs, "ID", "Title", rating.DiaDanhID);
            return View(rating);
        }

        //
        // POST: /Rating/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Rating rating)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rating).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DiaDanhID = new SelectList(db.DiaDanhs, "ID", "Title", rating.DiaDanhID);
            return View(rating);
        }

        //
        // GET: /Rating/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Rating rating = db.Ratings.Find(id);
            if (rating == null)
            {
                return HttpNotFound();
            }
            return View(rating);
        }

        //
        // POST: /Rating/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Rating rating = db.Ratings.Find(id);
            db.Ratings.Remove(rating);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}