﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project_LTW.Models
{
    public class Comment
    {
        public int ID { set; get; }
        //Nội dung
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự tối thiểu là 50", MinimumLength = 5)]
        public String Body { set; get; }
        //Ngày đăng
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        //Tác Giả
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        public String Author { set; get; }
        //Mail
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [DataType(DataType.EmailAddress)]
        public String Email { set; get; }
        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Minutes;
            }
        }
        public int Loai { set; get; }
        //1 comment của 1 bài viết về địa danh du lịch
        public int DiaDanhID { set; get; }
        public virtual DiaDanh DiaDanhs { set; get; }
        //1 comment của 1 bài viết về trung tâm du lịch
        public int TrungTamID { set; get; }
        public virtual TrungTam TrungTams { set; get; }
    }
}