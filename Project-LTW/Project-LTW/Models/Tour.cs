﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project_LTW.Models
{
    public class Tour
    {
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        public int TourID { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự tối thiểu là 50", MinimumLength = 5)]
        public String TenTour { set; get; }

        [DataType(DataType.Date)]
        public DateTime NgayDang { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự tối thiểu là 50", MinimumLength = 5)]
        public String NoiDi { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự tối thiểu là 50", MinimumLength = 5)]
        public String NoiDen { set; get; }

        [DataType(DataType.Date)]
        public DateTime NgayKhoiHanh { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        public long? Gia { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        public int? SoChoConNhan { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [DataType(DataType.Url)]
        public String UrlTour { set; get; }
    }
}