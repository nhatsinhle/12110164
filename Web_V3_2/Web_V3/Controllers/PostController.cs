﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_V3.Models;

namespace Web_V3.Controllers
{
    public class PostController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /Post/

        public ActionResult Index()
        {
            return View(db.Posts.ToList());
        }

        //
        // GET: /Post/Details/5

        public ActionResult Details(int id = 0)
        {
            Post post = db.Posts.Find(id);
            ViewData["idpost"] = id;
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Post post,string Content)
        {
            if (ModelState.IsValid)
            {
                post.DateCreated = DateTime.Now;
                post.DateUpdate = DateTime.Now;
                //Tao list các Tag
                List<Tag> Tags = new List<Tag>();
                //tach cac Tag theo dau ,
                string[] TagContent=Content.Split(',');
                //Lập các tag vừa tách
                foreach (string item in TagContent)
                {
                    //Tìm xem tag content đã có hay chưa
                    Tag TagExists = null;
                    var ListTag = db.Tags.Where(y => y.Content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        //nếu có tag rồi thì add thêm Post vào
                        TagExists = ListTag.First();
                        TagExists.Posts.Add(post);
                    }
                    else
                    {
                        //nếu chưa có tag thì tạo mới
                        TagExists = new Tag();
                        TagExists.Content = item;
                        TagExists.Posts = new List<Post>();
                        TagExists.Posts.Add(post);
                    }
                    //add vào List các tag
                    Tags.Add(TagExists);
                }
                //Gán List Tag cho Post
                post.Tags = Tags;
                //Tag tag1 = new Tag();
                //tag1.Content = Content;
                //if (post.Tags == null)
                //    post.Tags = new List<Tag>();
                //post.Tags.Add(tag1);
                //if (tag1.Posts == null)
                //    tag1.Posts = new List<Post>();
                //tag1.Posts.Add(post);
                post.Tags = Tags;
                db.Posts.Add(post);
                //db.Tags.Add(tag1);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(post);
        }

        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(post);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}