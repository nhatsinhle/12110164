﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QL_Account.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Diễn đàn InfoTorism - Nơi chia sẻ, cung cấp thông tin du lịch cho mọi người.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Diễn đàn du lịch - InfoTorism.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
