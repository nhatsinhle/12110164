﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QL_Account.Models;

namespace QL_Account.Controllers
{
    [Authorize]
    public class PostController : Controller
    {
        private BlogdDbContext db = new BlogdDbContext();

        //
        // GET: /Post/
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View(db.Posts.ToList());
        }

        //
        // GET: /Post/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            Post post = db.Posts.Find(id);
            ViewData["idpost"] = id;
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Post post, string Content)
        {
            if (ModelState.IsValid)
            {
                post.DateCreate = DateTime.Now;
                int userid=db.UserProfiles.Select(x=>new {x.UserId,x.UserName}).Where(y=>y.UserName==User.Identity.Name).Single().UserId;
                post.UserProfileID=userid;
                //db.User.Identity.Name
                //Tao list các Tag
                List<Tag> Tags = new List<Tag>();
                //tach cac Tag theo dau ,
                string[] TagContent = Content.Split(',');
                //Lập các tag vừa tách
                foreach (string item in TagContent)
                {
                    //Tìm xem tag content đã có hay chưa
                    Tag TagExists = null;
                    var ListTag = db.Tags.Where(y => y.Content.Equals(item.Trim()));
                    if (ListTag.Count() > 0)
                    {
                        //nếu có tag rồi thì add thêm Post vào
                        TagExists = ListTag.First();
                        TagExists.Posts.Add(post);
                    }
                    else
                    {
                        //nếu chưa có tag thì tạo mới
                        TagExists = new Tag();
                        TagExists.Content = item.Trim();
                        TagExists.Posts = new List<Post>();
                        TagExists.Posts.Add(post);
                    }
                    //add vào List các tag
                    Tags.Add(TagExists);
                }
                //Gán List Tag cho Post
                post.Tags = Tags;
                post.Tags = Tags;
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(post);
        }

        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(post);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}